/**
 * Created by aekachaiboonruang on 7/27/17.
 */
import Vue from 'vue'
/**
 * @param baseUrl can be base url with tailing / or port or targeted services
 * @param token use specified token instead of stored token
 */
export default function (baseUrl = 8080, hostname = '192.168.2.26', token = undefined) {
  if (!isNaN(baseUrl)) {
    baseUrl = 'http://' + window.location.hostname + ':' + baseUrl + '/'
  }
  const headers = {}
  if (!token) {
    token = localStorage.getItem('token') || sessionStorage.getItem('token')
  }
  if (token) {
    headers.Authorization = 'Bearer ' + token
  }
  const request = (method, path, body = '', params = {}) => Vue.http({
    method,
    url: baseUrl + path,
    params,
    body,
    headers
  })
    .then(res => res.body)
    .catch(err => {
      if (err.status === 401) {
        sessionStorage.removeItem('token')
        localStorage.removeItem('token')
        window.location.href = '/'
        return
      } else if (err.status === 403) {
        window.location.href = '/'
        return
      }
      // else if (err.status === 500) {
      //     alert("Error! Cannot get data")
      //     return
      // }
      throw err
    })
  return {
    get: (path, params = {}) => request('get', path, '', params),
    post: (path, body = {}) => request('post', path, body),
    put: (path, body = {}) => request('put', path, body),
    delete: (path, body = {}) => request('delete', path, body)
  }
}
export const getHttp = () => Vue.http
