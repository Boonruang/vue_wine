import Sidebar from './SideBar.vue'

const SidebarStore = {
  showSidebar: false,
  sidebarLinks: [
    {
      name: 'Admin',
      icon: 'ti-panel',
      path: '/admin/overview'
    },
    {
      name: 'Account',
      icon: 'ti-user',
      path: '/admin/stats'
    },
    {
      name: 'QR code',
      icon: 'ti-view-list-alt',
      path: '/admin/table-list'
    },
    {
      name: 'Pass code',
      icon: 'ti-text',
      path: '/admin/typography'
    }
  ],
  displaySidebar (value) {
    this.showSidebar = value
  }
}

const SidebarPlugin = {

  install (Vue) {
    Vue.mixin({
      data () {
        return {
          sidebarStore: SidebarStore
        }
      }
    })

    Object.defineProperty(Vue.prototype, '$sidebar', {
      get () {
        return this.$root.sidebarStore
      }
    })
    Vue.component('side-bar', Sidebar)
  }
}

export default SidebarPlugin
